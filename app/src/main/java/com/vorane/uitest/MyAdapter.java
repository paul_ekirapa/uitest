package com.vorane.uitest;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

/**
 * Created by hp on 4/27/2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> {
    private final Context context;
    ArrayList<String> names = new ArrayList<>();

    public MyAdapter(Context context, ArrayList<String> names) {
        this.context = context;
        this.names = names;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.button, parent, false);
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        String name = names.get(position);
        if (name.equals("Login")) {
            holder.button.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button1));
            holder.button.setText(name);
        } else if (name.equals("Clear")) {
            holder.button.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.button2));
            holder.button.setText(name);

        } else {
            holder.button.setText(name);
        }

    }

    @Override
    public int getItemCount() {
        Log.d("Size", "" + names.size());
        return names.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {
        Button button;

        public MyHolder(View itemView) {
            super(itemView);
            button = (Button) itemView.findViewById(R.id.button);
        }
    }

}
