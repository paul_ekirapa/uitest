package com.vorane.uitest;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    int SPAN_SIZE = 3;
    MyAdapter adapter;
    ArrayList<String> names = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        for (int i = 1; i < 10; i++) {
            names.add(i - 1, Integer.toString(i));
        }
        names.add(9, "Login");
        names.add(10, "0");
        names.add(11, "Clear");
        setRecycler();
    }

    private void setRecycler() {
        adapter = new MyAdapter(getBaseContext(), names);
        recyclerView = (RecyclerView) findViewById(R.id.buttons_recycler);
        // Check screen size
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            SPAN_SIZE = 3;
        }
        // Medium screen devices landscape orientation
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            Display display = ((WindowManager) getSystemService(WINDOW_SERVICE))
                    .getDefaultDisplay();
            int orientation = display.getRotation();
            if (orientation == Surface.ROTATION_90
                    || orientation == Surface.ROTATION_270) {
                SPAN_SIZE = 3;
            }
        }
        GridLayoutManager manager = new GridLayoutManager(getBaseContext(), SPAN_SIZE);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }
}
